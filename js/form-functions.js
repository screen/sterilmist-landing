var submitBtn = document.getElementById('submitBtn'),
    passd = true;

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function telephoneCheck(str) {
    var patt = new RegExp(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm);
    return patt.test(str);
}

submitBtn.addEventListener('click', function (e) {
    e.preventDefault();
    passd = true;

    /* NAME VALIDATION  */
    var inputText = document.getElementById('quoteFullName').value;
    var elements = document.getElementsByClassName('error-fullname');
    if (inputText == '' || inputText == null) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_nombre;
        passd = false;
    } else {
        if (inputText.length < 3) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = admin_url.invalid_nombre;
            passd = false;
        } else {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = '';
        }
    }

    /* EMAIL VALIDATION  */
    var inputText = document.getElementById('quoteEmail').value;
    var elements = document.getElementsByClassName('error-email');
    if (inputText == '' || inputText == null) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_email;
        passd = false;
    } else {
        if (validateEmail(inputText) == false) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = admin_url.invalid_email;
            passd = false;
        } else {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = '';
        }
    }

    /* PHONE VALIDATION  */
    var inputText = document.getElementById('quotePhone').value;
    var elements = document.getElementsByClassName('error-phone');
    if (inputText == '' || inputText == null) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_phone;
        passd = false;
    } else {
        if (telephoneCheck(inputText) == false) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = admin_url.invalid_phone;
            passd = false;
        } else {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = '';
        }
    }

    /* PHONE VALIDATION  */
    var inputText = document.getElementById('quoteAddress').value;
    var elements = document.getElementsByClassName('error-address');
    if (inputText == '' || inputText == null) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_address;
        passd = false;
    } else {

        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = '';
    }

    /* AIR CONDITIONER VALIDATION  */
    var inputText = document.getElementById('quoteAirUnits').value;
    var elements = document.getElementsByClassName('error-airunits');
    if (inputText == '' || inputText < 0) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_airunits;
        passd = false;
    } else {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = '';
    }

    /* AREA VALIDATION  */
    var inputText = document.getElementById('quoteArea').value;
    var elements = document.getElementsByClassName('error-area');
    if (inputText == '' || inputText < 0) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_area;
        passd = false;
    } else {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = '';
    }

    /* SELECT VALIDATION  */
    var inputText = document.getElementById('quoteSpaceType').value;
    var elements = document.getElementsByClassName('error-select');
    if (inputText == '' || inputText < 0) {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = admin_url.error_select;
        passd = false;
    } else {
        if (elements[0].classList.contains('d-none')) {
            elements[0].classList.toggle("d-none");
        }
        elements[0].innerHTML = '';
    }

    if (passd == true) {
        var info = 'action=send_quote_form&quote-fullname=' +
            document.getElementById('quoteFullName').value + '&quote-email=' + document.getElementById('quoteEmail').value + '&quote-phone=' + document.getElementById('quotePhone').value + '&quote-address=' + document.getElementById('quoteAddress').value + '&quote-airunits=' + document.getElementById('quoteAirUnits').value + '&quote-area=' + document.getElementById('quoteArea').value + '&quote-spacetype=' + document.getElementById('quoteSpaceType').value;
        var elements = document.getElementsByClassName('loader-css');
        elements[0].classList.toggle("d-none");
        /* SEND AJAX */
        newRequest = new XMLHttpRequest();
        newRequest.open('POST', admin_url.custom_ajax_url, true);
        newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        newRequest.onload = function () {
            var result = JSON.parse(newRequest.responseText);
            if (result.success == true) {
                window.location.replace(admin_url.thanks_url);
            } else {
                elements[0].classList.toggle("d-none");
                swal({
                    title: 'Error on Request',
                    text: result.data,
                    icon: 'error',
                    buttons: {
                        confirm: {
                            className: 'btn btn-danger',
                        },
                    },
                });
            }
        };
        newRequest.send(info);
    }

});
