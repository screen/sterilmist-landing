/* CUSTO ON LOAD FUNCTIONS */
function documentCustomLoad() {
    "use strict";
    var minusBtn = document.getElementsByClassName('quote-counter-number-minus'),
        plusBtn = document.getElementsByClassName('quote-counter-number-plus'),
        areaItems = document.getElementsByClassName('area-item'),
        typeItems = document.getElementsByClassName('type-item');

    for (var i = 0; i < minusBtn.length; i++) {
        minusBtn[i].addEventListener("click", quoteMinus, false);
    }

    for (var i = 0; i < plusBtn.length; i++) {
        plusBtn[i].addEventListener("click", quotePlus, false);
    }

    for (var i = 0; i < typeItems.length; i++) {
        typeItems[i].addEventListener("click", function () {
            var selectedElm = this.getAttribute("value"),
                span = document.getElementsByClassName('selected-item');
            for (var i = 0; i < span.length; ++i) {
                var item = span[i];
                item.innerHTML = selectedElm;
            }
            document.getElementById('quoteSpaceType').value = selectedElm;
        }, false);
    }

    for (var i = 0; i < areaItems.length; i++) {
        areaItems[i].addEventListener("click", function () {
            var selectedElm = this.getAttribute("value"),
                span = document.getElementsByClassName('selected-area');
            for (var i = 0; i < span.length; ++i) {
                var item = span[i];
                item.innerHTML = selectedElm;
            }
            document.getElementById('quoteArea').value = selectedElm;
        }, false);
    }

    AOS.init({
        // Global settings:
        disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
        startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
        initClassName: 'aos-init', // class applied after initialization
        animatedClassName: 'aos-animate', // class applied on animation
        useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
        disableMutationObserver: false, // disables automatic mutations' detections (advanced)
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
        throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        offset: 120, // offset (in px) from the original trigger point
        delay: 0, // values from 0 to 3000, with step 50ms
        duration: 400, // values from 0 to 3000, with step 50ms
        easing: 'ease', // default easing for AOS animations
        once: false, // whether animation should happen only once - while scrolling down
        mirror: false, // whether elements should animate out while scrolling past them
        anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
    });

    var gallerySwiper = new Swiper('.gallery-swiper', {
        // Optional parameters
        speed: 400,
        spaceBetween: 0,
        direction: 'horizontal',
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        slidesPerView: 3,
        breakpoints: {
            0: {
                slidesPerView: 1
            },
            768: {
                slidesPerView: 2
            },
            991: {
                slidesPerView: 3
            }
        }
    });

    var reviewSwiper = new Swiper('.review-swiper', {
        // Optional parameters
        speed: 400,
        spaceBetween: 0,
        direction: 'horizontal',
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        slidesPerView: 1
    });
}

/* FUNCTIONS FOR FORM */
function quoteMinus() {
    var x = document.getElementById("quoteAirUnits").value;
    if (x == 0) {
        x = 1;
    } else {
        x = parseInt(x) - 1;
    }
    document.getElementById("quoteAirUnits").value = x;
}

function quotePlus() {
    var x = document.getElementById("quoteAirUnits").value;
    if (x == '') {
        x = 1;
    } else {
        x = parseInt(x) + 1;
    }
    document.getElementById("quoteAirUnits").value = x;
}



document.addEventListener("DOMContentLoaded", documentCustomLoad, false);
