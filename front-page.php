<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="the-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center row-hero">
                    <div class="hero-content col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12" data-aos="fadeIn" data-aos-delay="50">
                        <?php $title = get_post_meta(get_the_ID() ,'smt_hero_title', true); ?>
                        <?php $subtitle = get_post_meta(get_the_ID() ,'smt_hero_subtitle', true); ?>
                        <?php $content = get_post_meta(get_the_ID() ,'smt_hero_description', true); ?>

                        <?php echo apply_filters('the_content', $title); ?>
                        <h2><?php echo $subtitle; ?></h2>

                        <div class="hero-content-text">
                            <?php echo apply_filters('the_content', $content); ?>
                        </div>

                    </div>
                    <div class="hero-image col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1" data-aos="fadeIn" data-aos-delay="150">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'smt_hero_bg_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>
                </div>
            </div>
        </section>

        <section class="the-quote col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="quote-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 class="section-title" data-aos="fadeIn" data-aos-delay="50"><?php echo get_post_meta(get_the_ID(), 'smt_quote_title', true); ?></h2>
                        <div class="section-description" data-aos="fadeIn" data-aos-delay="150">
                            <?php $content = get_post_meta(get_the_ID() ,'smt_quote_description', true); ?>
                            <?php echo apply_filters('the_content', $content); ?>
                        </div>
                        <div class="row">
                            <div class="quote-form-container col-12" data-aos="fadeIn" data-aos-delay="50">
                                <?php get_template_part('templates/quote-form'); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="the-services col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-between">
                    <?php $services_group = get_post_meta(get_the_ID(), 'smt_home_service_group', true); ?>
                    <?php if (!empty($services_group)) { ?>
                    <?php $i = 1; ?>
                    <?php foreach ($services_group as $service_item) { ?>
                    <article class="services-item col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="<?php echo $i * 50; ?>">
                        <h2><?php echo $service_item['title']; ?></h2>
                        <?php echo apply_filters('the_content', $service_item['description']); ?>
                    </article>
                    <?php $i++; } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="the-promo col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="promo-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="50">
                        <h2><?php echo get_post_meta(get_the_ID(), 'smt_promo_title', true); ?></h2>

                        <div class="promo-countdown-container">
                            <?php get_template_part('templates/countdown-timer'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-gallery col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="50">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="gallery-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="gallery-swiper swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <?php $raw_gallery = get_post_meta(get_the_ID(), 'smt_home_gallery', true); ?>
                                <?php foreach ( (array) $raw_gallery as $attachment_id => $attachment_url ) { ?>
                                <?php $image = wp_get_attachment_image_src($attachment_id, 'full' ); ?>
                                <div class="swiper-slide">
                                    <img src="<?php echo $image[0];?>" alt="Gallery item" loading="lazy" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <section class="the-certificate col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start justify-content-between">
                    <div class="certficate-text col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12" data-aos="fadeIn" data-aos-delay="50">
                        <?php echo get_post_meta(get_the_ID(), 'smt_cert_title', true); ?>

                        <a href="<?php echo get_post_meta(get_the_ID(), 'smt_home_cert_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'smt_home_cert_button_text', true); ?>" class="btn btn-md btn-certificate"><?php echo get_post_meta(get_the_ID(), 'smt_home_cert_button_text', true); ?></a>
                    </div>
                    <div class="certficate-img col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1" data-aos="fadeIn" data-aos-delay="150">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'smt_cert_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" loading="lazy" />
                    </div>
                </div>
            </div>
        </section>

        <section class="the-reviews col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="reviews-title col-xl-7 col-lg-7 col-md-8 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="50">
                        <h2 class="section-title"> <?php echo get_post_meta(get_the_ID(), 'smt_home_reviews_title', true); ?></h2>
                    </div>
                    <div class="reviews-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $local_review = get_post_meta(get_the_ID(), 'smt_home_reviews_activate', true); ?>
                        <?php if ($local_review == 'on') { ?>
                        <div class="row">
                            <div class="review-logo col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="50">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logos_google.png" alt="Google Reviews" loading="lazy" class="img-fluid" />
                                <div class="star-logo-container">
                                    <span>5.0</span> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                </div>
                            </div>
                            <div class="review-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="150">
                                <div class="review-swiper swiper-container">
                                    <!-- Additional required wrapper -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <?php $reviews = get_post_meta(get_the_ID(), 'smt_home_reviews_group', true); ?>
                                        <?php foreach ($reviews as $items) { ?>
                                        <div class="swiper-slide">
                                            <div class="review-description">
                                                <?php echo apply_filters('the_content', $items['description']); ?>
                                            </div>
                                            <div class="review-name">
                                                <h3><?php echo $items['title']; ?></h3>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <!-- If we need navigation buttons -->
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>
                                </div>
                            </div>
                        </div>
                        <?php } else { ?>
                        Google Reviews
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-logos col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-between">
                    <?php $raw_gallery = get_post_meta(get_the_ID(), 'smt_home_logos_gallery', true); ?>
                    <?php  $i = 1; ?>
                    <?php if (!empty($raw_gallery)) { ?>
                    <?php foreach ( (array) $raw_gallery as $attachment_id => $attachment_url ) { ?>
                    <?php $image = wp_get_attachment_image_src($attachment_id, 'full' ); ?>
                    <div class="gallery-item col-xl-2 col-lg-2 col-md-3 col-sm col" data-aos="fadeIn" data-aos-delay="<?php echo $i * 100; ?>">
                        <img src="<?php echo $image[0];?>" alt="Logo item" loading="lazy" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                    </div>
                    <?php $i++; } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="home-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="50">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="home-footer-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'smt_home_footer_bg_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>"  loading="lazy"/>
                        <div class="home-footer-wrapper">
                            <div data-aos="fadeIn" data-aos-delay="250">
                                <?php $content = get_post_meta(get_the_ID() ,'smt_home_footer_title', true); ?>
                                <?php echo apply_filters('the_content', $content); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
