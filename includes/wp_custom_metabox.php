<?php
function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}
add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );

add_action( 'cmb2_admin_init', 'sterilmist_register_custom_metabox' );
function sterilmist_register_custom_metabox() {
    $prefix = 'smt_';

    /*------------ 1.- HOME ------------*/
    $cmb_home_hero = new_cmb2_box( array(
        'id'            => $prefix . 'home_hero',
        'title'         => esc_html__( 'Home: Hero Principal', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_hero->add_field( array(
        'id'   => $prefix . 'hero_bg',
        'name'      => esc_html__( 'Imagen de Fondo del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'sterilmist' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'sterilmist' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_hero->add_field( array(
        'id'   => $prefix . 'hero_title',
        'name'      => esc_html__( 'Título del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_home_hero->add_field( array(
        'id'   => $prefix . 'hero_subtitle',
        'name'      => esc_html__( 'Subtítulo del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'text'
    ) );

    $cmb_home_hero->add_field( array(
        'id'   => $prefix . 'hero_description',
        'name'      => esc_html__( 'Descripción del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa una descripción descriptiva para el Hero', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    /*------------ 2.- MISSION ------------*/
    $cmb_home_quote = new_cmb2_box( array(
        'id'            => $prefix . 'home_quote',
        'title'         => esc_html__( 'Home: Quote', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_quote->add_field( array(
        'id'   => $prefix . 'quote_title',
        'name'      => esc_html__( 'Título de la Sección', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para la sección', 'sterilmist' ),
        'type' => 'text'
    ) );

    $cmb_home_quote->add_field( array(
        'id'   => $prefix . 'quote_description',
        'name'      => esc_html__( 'Descripción de la Sección', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa una descripción descriptiva para la sección', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    /*------------ 3.- SERVICES ------------*/
    $cmb_home_services = new_cmb2_box( array(
        'id'            => $prefix . 'home_services',
        'title'         => esc_html__( 'Home: Services Section', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $group_field_id = $cmb_home_services->add_field( array(
        'id'          => $prefix . 'home_service_group',
        'type'        => 'group',
        'description' => __( 'Descriptive area of Services', 'sterilmist' ),
        'options'     => array(
            'group_title'       => __( 'Service {#}', 'sterilmist' ),
            'add_button'        => __( 'Add Service', 'sterilmist' ),
            'remove_button'     => __( 'Remove Service', 'sterilmist' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'are you sure to delete this Service?', 'sterilmist' )
        )
    ) );

    $cmb_home_services->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Título del Servicio', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un título descriptivo para este servicio', 'sterilmist' ),
        'type' => 'text'
    ) );

    $cmb_home_services->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Descripción del Servicio', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un texto descriptivo para este servicio', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    /*------------ 4.- PROMOTIONS ------------*/
    $cmb_home_promo = new_cmb2_box( array(
        'id'            => $prefix . 'home_promo',
        'title'         => esc_html__( 'Home: Promotion Section', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_promo->add_field( array(
        'id'   => $prefix . 'promo_title',
        'name'      => esc_html__( 'Promo Title', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'text'
    ) );

    $cmb_home_promo->add_field( array(
        'id'   => $prefix . 'promo_date',
        'name'      => esc_html__( 'Promo Date', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'text_date'
        // 'timezone_meta_key' => 'wiki_test_timezone',
        // 'date_format' => 'l jS \of F Y',
    ) );

    /*------------ 5.- GALLERY ------------*/
    $cmb_home_promo = new_cmb2_box( array(
        'id'            => $prefix . 'home_gallery_metabox',
        'title'         => esc_html__( 'Home: Gallery Section', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_promo->add_field( array(
        'id'   => $prefix . 'home_gallery',
        'name'      => esc_html__( 'Promo Title', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => esc_html__( 'Add or Remove Images', 'sterilmist' ),
            'remove_image_text' => esc_html__( 'Remove Images', 'sterilmist' ),
            'file_text' => esc_html__( 'Image', 'sterilmist' ),
            'file_download_text' => esc_html__( 'Download', 'sterilmist' ),
            'remove_text' => esc_html__( 'Remove', 'sterilmist' ),
        ),
    ) );


    /*------------ 6.- CERTIFICATE ------------*/
    $cmb_home_cert = new_cmb2_box( array(
        'id'            => $prefix . 'home_cert_metabox',
        'title'         => esc_html__( 'Home: Certificates', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_cert->add_field( array(
        'id'   => $prefix . 'cert_image',
        'name'      => esc_html__( 'Imagen de Fondo del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'sterilmist' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'sterilmist' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_cert->add_field( array(
        'id'   => $prefix . 'cert_title',
        'name'      => esc_html__( 'Título del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_home_cert->add_field( array(
        'id'             => $prefix . 'home_cert_button_text',
        'name'      => esc_html__( 'Button Text', 'maxicon' ),
        'desc'      => esc_html__( 'Enter a descriptive text for this button', 'maxicon' ),
        'type'      => 'text'
    ) );

    $cmb_home_cert->add_field( array(
        'id'             => $prefix . 'home_cert_button_url',
        'name'      => esc_html__( 'Button URL', 'maxicon' ),
        'desc'      => esc_html__( 'Enter the URL action for this button', 'maxicon' ),
        'type'      => 'text_url'
    ) );

    /*------------ 7. REVIEWS ------------*/
    $cmb_home_reviews = new_cmb2_box( array(
        'id'            => $prefix . 'home_reviews_metabox',
        'title'         => esc_html__( 'Home: Promotion Section', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_reviews->add_field( array(
        'id'   => $prefix . 'home_reviews_title',
        'name'      => esc_html__( 'Reviews Title', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'text'
    ) );

    $cmb_home_reviews->add_field( array(
        'id'   => $prefix . 'home_reviews_activate',
        'name'      => esc_html__( 'Activate Local Reviews', 'sterilmist' ),
        'desc'      => esc_html__( 'Activate this if you want Local Reviews instead of Google Reviews', 'sterilmist' ),
        'type' => 'checkbox'
    ) );

    $group_field_id = $cmb_home_reviews->add_field( array(
        'id'          => $prefix . 'home_reviews_group',
        'type'        => 'group',
        'description' => __( 'Local Reviews', 'sterilmist' ),
        'options'     => array(
            'group_title'       => __( 'Review {#}', 'sterilmist' ),
            'add_button'        => __( 'Add Review', 'sterilmist' ),
            'remove_button'     => __( 'Remove Review', 'sterilmist' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'are you sure to delete this Review?', 'sterilmist' )
        )
    ) );

    $cmb_home_reviews->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Name', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un título descriptivo para este servicio', 'sterilmist' ),
        'type' => 'text'
    ) );

    $cmb_home_reviews->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Descriptive Review', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un texto descriptivo para este servicio', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    /*------------ 8.- LOGO CERTIFICATES ------------*/
    $cmb_home_logo = new_cmb2_box( array(
        'id'            => $prefix . 'home_logos_metabox',
        'title'         => esc_html__( 'Home: Logos Section', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_logo->add_field( array(
        'id'   => $prefix . 'home_logos_gallery',
        'name'      => esc_html__( 'Logo Images', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => esc_html__( 'Add or Remove Images', 'sterilmist' ),
            'remove_image_text' => esc_html__( 'Remove Images', 'sterilmist' ),
            'file_text' => esc_html__( 'Image', 'sterilmist' ),
            'file_download_text' => esc_html__( 'Download', 'sterilmist' ),
            'remove_text' => esc_html__( 'Remove', 'sterilmist' ),
        ),
    ) );

    /*------------ 9.- CERTIFICATE ------------*/
    $cmb_home_footer = new_cmb2_box( array(
        'id'            => $prefix . 'home_footer_metabox',
        'title'         => esc_html__( 'Home: Footer Hero', 'sterilmist' ),
        'object_types'  => array( 'page' ),
        'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_home_footer->add_field( array(
        'id'   => $prefix . 'home_footer_bg_image',
        'name'      => esc_html__( 'Imagen de Fondo del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'sterilmist' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'sterilmist' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_footer->add_field( array(
        'id'   => $prefix . 'home_footer_title',
        'name'      => esc_html__( 'Título del Hero', 'sterilmist' ),
        'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'sterilmist' ),
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );
}
