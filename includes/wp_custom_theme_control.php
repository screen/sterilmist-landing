<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action( 'customize_register', 'sterilmist_customize_register' );

function sterilmist_customize_register( $wp_customize ) {
    /* HEADER */
    $wp_customize->add_section('smt_header_settings', array(
        'title'    => __('Header', 'sterilmist'),
        'description' => __('Options for Header Elements', 'sterilmist'),
        'priority' => 30
    ));

    $wp_customize->add_setting('smt_header_settings[phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'phone_number', array(
        'type' => 'text',
        'label'    => __('Phone Number', 'sterilmist'),
        'description' => __( 'Add phone number link for header button', 'sterilmist' ),
        'section'  => 'smt_header_settings',
        'settings' => 'smt_header_settings[phone_number]'
    ));

    $wp_customize->add_setting('smt_header_settings[header_button_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'header_button_text', array(
        'type' => 'text',
        'label'    => __('Button Text', 'sterilmist'),
        'description' => __( 'Add text for Header Button Link', 'sterilmist' ),
        'section'  => 'smt_header_settings',
        'settings' => 'smt_header_settings[header_button_text]'
    ));

    $wp_customize->add_setting('smt_header_settings[header_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'header_text', array(
        'type' => 'text',
        'label'    => __('Header Text', 'sterilmist'),
        'description' => __( 'Add additional Header Text', 'sterilmist' ),
        'section'  => 'smt_header_settings',
        'settings' => 'smt_header_settings[header_text]'
    ));

    /* SOCIAL */
    $wp_customize->add_section('smt_social_settings', array(
        'title'    => __('Redes Sociales', 'sterilmist'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'sterilmist'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('smt_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'sterilmist_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'smt_social_settings',
        'settings' => 'smt_social_settings[facebook]',
        'label' => __( 'Facebook', 'sterilmist' ),
    ) );

    $wp_customize->add_setting('smt_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'sterilmist_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'smt_social_settings',
        'settings' => 'smt_social_settings[twitter]',
        'label' => __( 'Twitter', 'sterilmist' ),
    ) );

    $wp_customize->add_setting('smt_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'sterilmist_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'smt_social_settings',
        'settings' => 'smt_social_settings[instagram]',
        'label' => __( 'Instagram', 'sterilmist' ),
    ) );

    $wp_customize->add_setting('smt_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'sterilmist_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'smt_social_settings',
        'settings' => 'smt_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'sterilmist' ),
    ) );

    $wp_customize->add_setting('smt_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'sterilmist_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'smt_social_settings',
        'settings' => 'smt_social_settings[youtube]',
        'label' => __( 'YouTube', 'sterilmist' ),
    ) );

    $wp_customize->add_setting('smt_social_settings[yelp]', array(
        'default'           => '',
        'sanitize_callback' => 'sterilmist_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'yelp', array(
        'type' => 'url',
        'section' => 'smt_social_settings',
        'settings' => 'smt_social_settings[yelp]',
        'label' => __( 'Yelp', 'sterilmist' ),
    ) );


    $wp_customize->add_section('smt_cookie_settings', array(
        'title'    => __('Cookies', 'sterilmist'),
        'description' => __('Opciones de Cookies', 'sterilmist'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('smt_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'sterilmist'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'smt_cookie_settings',
        'settings' => 'smt_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('smt_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'smt_cookie_settings',
        'settings' => 'smt_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'sterilmist' ),
    ) );

}

function sterilmist_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

