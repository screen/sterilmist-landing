<?php

function sterilmist_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'sterilmist' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'sterilmist' ),
		'menu_name'             => __( 'Clientes', 'sterilmist' ),
		'name_admin_bar'        => __( 'Clientes', 'sterilmist' ),
		'archives'              => __( 'Archivo de Clientes', 'sterilmist' ),
		'attributes'            => __( 'Atributos de Cliente', 'sterilmist' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'sterilmist' ),
		'all_items'             => __( 'Todos los Clientes', 'sterilmist' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'sterilmist' ),
		'add_new'               => __( 'Agregar Nuevo', 'sterilmist' ),
		'new_item'              => __( 'Nuevo Cliente', 'sterilmist' ),
		'edit_item'             => __( 'Editar Cliente', 'sterilmist' ),
		'update_item'           => __( 'Actualizar Cliente', 'sterilmist' ),
		'view_item'             => __( 'Ver Cliente', 'sterilmist' ),
		'view_items'            => __( 'Ver Clientes', 'sterilmist' ),
		'search_items'          => __( 'Buscar Cliente', 'sterilmist' ),
		'not_found'             => __( 'No hay resultados', 'sterilmist' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'sterilmist' ),
		'featured_image'        => __( 'Imagen del Cliente', 'sterilmist' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'sterilmist' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'sterilmist' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'sterilmist' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'sterilmist' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'sterilmist' ),
		'items_list'            => __( 'Listado de Clientes', 'sterilmist' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'sterilmist' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'sterilmist' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'sterilmist' ),
		'description'           => __( 'Portafolio de Clientes', 'sterilmist' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'sterilmist_custom_post_type', 0 );