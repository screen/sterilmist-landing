<form id="quoteFormContainer" class="quote-form-content">
    <div class="container">
        <div class="row">
            <div class="quote-form-control col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="quoteFullName" name="quote-fullname" type="text" class="form-control" placeholder="<?php _e('Full Name', 'sterilmist'); ?>" />
                <small class="error error-fullname d-none"></small>
            </div>
            <div class="quote-form-control col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="quoteEmail" name="quote-email" type="email" class="form-control" placeholder="<?php _e('Email', 'sterilmist'); ?>" />
                <small class="error error-email d-none"></small>
            </div>
            <div class="quote-form-control col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="quotePhone" name="quote-phone" type="tel" class="form-control" placeholder="<?php _e('Phone (E.G. +1(305)9301162)', 'sterilmist'); ?>" />
                <small class="error error-phone d-none"></small>
            </div>
            <div class="quote-form-control col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="quoteAddress" name="quote-address" type="text" class="form-control" placeholder="<?php _e('Complete Adddress', 'sterilmist'); ?>" />
                <small class="error error-address d-none"></small>
            </div>
            <div class="quote-form-control quote-form-special col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3><?php _e('How many air conditioner units do you have?', 'sterilmist'); ?></h3>
                <div class="quote-counter-control quote-counter-number">
                    <span class="quote-counter-number-minus">-</span>
                    <input id="quoteAirUnits" type="number" placeholder="00" name="quote-airunits" class="form-control counter-form-control">
                    <span class="quote-counter-number-plus">+</span>
                </div>
                <small class="error error-airunits d-none"></small>
            </div>
            <div class="quote-form-control quote-form-special col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3><?php _e('Approximate area you want to disinfect in square footage?', 'sterilmist'); ?></h3>
                <div class="quote-counter-select-control">
                    <input id="quoteArea" type="hidden" name="quote-area" class="form-control counter-form-control">
                    <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="selected-area">00 sqft</span>
                        </a>
                        <div id="quoteAreaSelector" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="area-item dropdown-item" value="500-1,000 Sq Ft.">500-1,000 Sq Ft.</a>
                            <a class="area-item dropdown-item" value="1,000-2,000 Sq Ft.">1,000-2,000 Sq Ft.</a>
                            <a class="area-item dropdown-item" value="2,000-3,000 Sq Ft.">2,000-3,000 Sq Ft.</a>
                            <a class="area-item dropdown-item" value="3,000-4,000 Sq Ft.">3,000-4,000 Sq Ft.</a>
                            <a class="area-item dropdown-item" value="4,000-5,000 Sq Ft.">4,000-5,000 Sq Ft.</a>
                        </div>
                    </div>
                </div>
                <small class="error error-area d-none"></small>
            </div>

            <div class="quote-form-control quote-form-special col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3><?php _e('What type of space do you want to disinfect?', 'sterilmist'); ?></h3>
                <div class="quote-counter-select-control">
                    <input id="quoteSpaceType" type="hidden" name="quote-spacetype" value="" />
                    <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="selected-item">Select</span>
                        </a>
                        <div id="quoteSpaceTypeSelector" class="dropdown-menu" aria-labelledby="type-item dropdownMenuLink">
                            <a class="type-item dropdown-item" value="house">House</a>
                            <a class="type-item dropdown-item" value="office">Office</a>
                            <a class="type-item dropdown-item" value="retail">Retail</a>
                            <a class="type-item dropdown-item" value="restaurant">Restaurant</a>
                            <a class="type-item dropdown-item" value="realtor">Realtor</a>
                            <a class="type-item dropdown-item" value="other">Other</a>
                        </div>
                    </div>
                </div>
                <small class="error error-select d-none"></small>
            </div>

            <div class="quote-form-control quote-submit-control col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="loader-css d-none"></div>
                <button id="submitBtn" type="submit" class="btn btn-md btn-quote"><?php _e('Get a Quote', 'sterilmist'); ?></button>
            </div>

            <div class="quote-form-control col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h4>OR Talk to a Cleaning Specialist <strong>TODAY</strong></h4>
                <?php $header_options = get_option('smt_header_settings'); ?>
                <a href="<?php echo $header_options['phone_number']; ?>" title="<?php echo $header_options['header_button_text']; ?>" class="btn btn-md btn-quote-number"><?php echo $header_options['header_button_text']; ?></a>
            </div>
        </div>
    </div>
</form>
