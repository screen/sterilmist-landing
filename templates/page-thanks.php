<?php
/**
* Template Name: Template - Thanks
*
* @package sterilmist
* @subpackage sterilmist-mk01-theme
* @since Mk. 1.0
*/
?>

<?php get_header(); ?>
<?php the_post(); ?>
<?php $front_page_id = get_option('page_on_front'); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="the-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center row-thanks justify-content-center">
                    <div class="hero-content col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                        <div class="animation-ctn">
                            <div class="icon icon--order-success svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="154px" height="154px">
                                    <g fill="none" stroke="#22AE73" stroke-width="2">
                                        <circle cx="77" cy="77" r="72" style="stroke-dasharray:480px, 480px; stroke-dashoffset: 960px;"></circle>
                                        <circle id="colored" fill="#22AE73" cx="77" cy="77" r="72" style="stroke-dasharray:480px, 480px; stroke-dashoffset: 960px;"></circle>
                                        <polyline class="st0" stroke="#fff" stroke-width="10" points="43.5,77.8 63.7,97.9 112.2,49.4 " style="stroke-dasharray:100px, 100px; stroke-dashoffset: 200px;" />
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <?php the_content(); ?>
                        <a href="<?php echo home_url('/'); ?>" title="<?php _e('Click here to back to home', 'sterilmist'); ?>" class="btn btn-md btn-back"><?php _e('Back to Home', 'sterilmist'); ?></a>
                    </div>

                </div>
            </div>
        </section>

        <section class="the-gallery gallery-thanks col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="gallery-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="gallery-swiper swiper-container">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <?php $raw_gallery = get_post_meta($front_page_id, 'smt_home_gallery', true); ?>
                                <?php foreach ( (array) $raw_gallery as $attachment_id => $attachment_url ) { ?>
                                <?php $image = wp_get_attachment_image_src($attachment_id, 'full' ); ?>
                                <div class="swiper-slide">
                                    <img src="<?php echo $image[0];?>" alt="Gallery item" class="img-fluid" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>
</main>
<?php get_footer(); ?>
